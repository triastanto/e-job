@extends('layouts.adminLTE')
@section('style')
<style>
    .page-break {
        page-break-after: always;
    }

    .jobdes {
        font-family:  Tahoma;
        font-size: 12pt;
        border-collapse: collapse;
        border-spacing: 0;
        /* width: 100%; */
        border: 1px solid #ddd;
        width: 100%;
        /* width: 21cm; */
        margin-left: auto; 
  margin-right: auto;
        
    }

    .jobdes td.isi {
        font-size: 11px;
        padding: 10px;
        text-align: justify;
    }

    .jobdes tr.isi {
        font-size: 11px;
        padding: 10px;
    }

    .jobdes td.judul {
        font-size: 12px;
        padding: 10px;
    }

    td.subjudul {
         font-family:  Tahoma;
        padding: 10px;
        font-size: 12px;
        border: 1px solid #ddd;
    }

    .isiprofil {
        font-size: 11px;
         font-family:  Tahoma;
        padding: 10px;
    }

    .judulprofil {
        font-size: 12px;
         font-family:  Tahoma;
        padding: 10px;
    }
</style>
@endsection
@section('content')
<section class="content">
    <div class="box box-warning" align="right">
        <div class="box-body"><a href="{{ url('cetakpdf',['id'=>$id]) }}" class="btn btn-default"><i class="fa fa-print"></i>
                Print</a>
            &nbsp; <a class="btn btn-warning pull-right" href="javascript:history.back()"><i class="fa fa-reply"></i>
                Kembali
            </a>
        </div>
    </div>
    <div id="cetak">
        <div class="box box-warning">
            <div class="box-body">
                <body >
                    <img alt="Gambar Koala" src="{{ url('img/kstulisan.png') }}"  width="300" /><br>&nbsp;
                </body>
                
                <table class="jobdes" border="1" >
                    <tr>
                        <td class="isi" width="15%">Record Sheet No.</td>
                        <td class="isi" align="center" width="1%">:</td>
                        <td class="isi">RS/PO01/001-ISSUE No.3</td>
                        <td rowspan="3" class="judul" align="center" width="25%"><b>URAIAN JABATAN<br>(Job Description)</b></td>
                        <td class="isi">Halaman(Page)</td>
                        <td class="isi" align="center" width="1%">:</td>
                        <td class="isi"></td>
                    </tr>
                    <tr>
                        <td class="isi">Issue Date</td>
                        <td class="isi" align="center">:</td>
                        <td class="isi">01/06/2010</td>
                        <td class="isi" rowspan="2">Tgl. Berlaku(Validity Date)</td>
                        <td class="isi" rowspan="2" align="center">:</td>
                        <td class="isi" rowspan="2">{{date('d/m/Y',strtotime($data[0]->tglapproveodhcp))}}</td>
                    </tr>
                    <tr>
                        <td class="isi">Holder</td>
                        <td class="isi" align="center">:</td>
                        <td class="isi">Divisi OD&HCP</td>
                    </tr>
                </table>
                <br>
<table class="jobdes">

                    <tr>
                        <td colspan="7" class="subjudul"><b>I.IDENTIFIKASI JABATAN (Job Identification)</b></td>
                    </tr>
                    <tr>
                        <td colspan="7">
                            <table class="jobdes" style="padding:5px">
                                <tr class="isi" >
                                    <td width="25%" class="isi">No. Jabatan (Job No)</td>
                                    <td align="center" width="2%" class="isi">:</td>
                                    <td class="isi">{{$data[0]->no_jabatan}}</td>
                                    <td colspan="2" class="isi">Gol.Jabatan (Job Level) : {{$data[0]->gol_jabatan}}</td>
                                </tr>
                                <tr class="isi">
                                    <td width="25%" class="isi">Nama Jabatan (Job Name)</td>
                                    <td align="center" width="2%" class="isi">:</td>
                                    <td colspan="3" class="isi">{{$data[0]->name_jabatan}}</td>
                                </tr>
                                <tr class="isi">
                                    <td width="25%" class="isi">Dinas (Official)</td>
                                    <td align="center" width="2%" class="isi">:</td>
                                    <td colspan="3" class="isi">{{$data[0]->dinas}}</td>
                                </tr>
                                <tr class="isi">
                                    <td width="25%" class="isi">Divisi (Division)</td>
                                    <td align="center" width="2%" class="isi">:</td>
                                    <td colspan="3" class="isi">{{$data[0]->divisi}}</td>
                                </tr>
                                <tr class="isi">
                                    <td width="25%" class="isi">Subdirektorat (Subdirectorate)</td>
                                    <td align="center" width="2%" class="isi">:</td>
                                    <td colspan="3" class="isi">{{$data[0]->subdirektorat}}</td>
                                </tr>
                                {{-- <tr class="isi">
                                    <td width="25%">Bertanggung jawab langsung kepada (Directly Responsible to)</td>
                                    <td align="center" width="2%">:</td>
                                    <td colspan="3">{{$data[0]->jabatanatasanlangsung}}</td>
                                </tr> --}}


                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="7">
                            <table width="100%">
                                <tr class="isi">
                                    <td class="isi" width="26%">Bertanggung jawab langsung kepada<br>(Directly Responsible to)</td>
                                    <td class="isi" width="2%">:</td>
                                    <td class="isi"> {{$data[0]->jabatanatasanlangsung}}</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="7">
                            <table width="100%">
                                <tr class="isi">
                                    <td width="26%" class="isi">Jabatan yang diawasi langsung<br>(Direct Supervised positions)</td>
                                    <td class="isi" width="2%">:</td>
                                    <td class="isi">
                                        @foreach ($job as $koor)

                                        <li> {{$koor->jabatanbawahanlangsung}}</li>

                                        @endforeach

                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr style="border: 1px solid #ddd;">
                        <td colspan="7" class="subjudul"><b>II.TUJUAN JABATAN (Primary Job Role)</b></td>
                    </tr>
                    <tr>
                        <td colspan="7" class="isi">{{$data[0]->jobrole}}</td>
                    </tr>
                    <tr>
                        <td colspan="7" class="subjudul"><b>III.TANGGUNG JAWAB UTAMA (Main Responsibility)</b></td>
                    </tr>
                    <tr>
                        <td colspan="4" class="subjudul" align="center"><b>Tugas & Tanggung jawab<br>(Duties &
                                Responsibilities)</b></td>
                        <td colspan="3" class="subjudul" align="center"><b>Indikator Capaian<br>(Performance
                                Indicators)</b></td>
                    </tr>
                    <?php $no=0; ?>
                    @foreach ($jobres as $jres)

                    <tr>
                        <td colspan="4" class="isi">{{$jres->keterangan." ".lcfirst($jres->object)}}</td>
                        <td colspan="3" class="isi">{{$jres->id_met_indikator}}</td>
                    </tr>
                    <?php $no++; ?>
                    @endforeach
                    <tr>
                        <td colspan="4" class="isi">Melaksanakan setiap tugas dan tanggung jawab dengan memerhatikan
                            aspek dan kondisi
                            keuangan
                            perusahaan serta mendukung program penghematan perusahaan.
                        </td>
                        <td colspan="3" class="isi">Penghematan Unit Kerja
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" class="isi">Melaksanakan tugas sesuai dengan Sistem Manajemen Krakatau Steel
                            (SMKS) dan
                            peraturan/kebijakan
                            yang berlaku baik internal (Prosedur, WI, SOP, PKB) maupun eksternal (Perpres, Permen,
                            Kepmen,
                            dll) untuk menghasilkan kualitas kerja yang tinggi dan memenuhi standar yang ditetapkan.
                        </td>
                        <td colspan="3" class="isi">Pelaksanaan kerja sesuai proses bisnis perusahaan serta peraturan
                            yang berlaku
                        </td>
                    </tr>
                    <tr>
                        <td colspan="7" class="subjudul"><b>IV. DIMENSI (Dimensions)</b></td>
                    </tr>
                    <tr>
                        <td colspan="7">
                            <table width="100%">
                                <tr class="isi">
                                    <td width="30%"  class="isi">Finansial (Financial)</td>
                                    <td align="center" width="2%"  class="isi">:</td>
                                    {{-- <td class="isi">{{$data[0]->finansial}}</td> --}}
                                    <td class="isi"> 
                                        @foreach(explode(',', $data[0]->finansial) as $info) 
                                            &#9679; {{$info}}<br>
                                        @endforeach    
                                    </td>
                                </tr>
                                <tr class="isi">
                                    <td width="30%" class="isi">Non Finansial (Financial)</td>
                                    <td align="center" width="2%" class="isi">:</td>
                                    {{-- <td class="isi">{{$data[0]->nonfinansial}}</td> --}}
                                    <td class="isi">
                                        @foreach(explode(',', $data[0]->nonfinansial) as $info) 
                                            &#9679; {{$info}}<br>
                                        @endforeach    
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="7" class="subjudul"><b>V. WEWENANG (Authorities)</b></td>
                    </tr>
                    <tr>
                        <td colspan="7" class="isi">
                            <table width="100%" border="0">
                               

                                <tr class="isi">
                                    <td class="isi">
                                     @foreach ($jobres as $jres2)
                                   <li style="list-style-type:decimal;">{{$jres2->id_met_kewenangan}}</li>
                                     @endforeach
                                    </td>
                                    </tr>
                                
                               
                            </table>
                        </td>
                    </tr>
                    <tr>
                            <td colspan="7" class="subjudul"><b>VI. HUBUNGAN KERJA (Work Relationship)</b></td>
                        </tr>
                        <tr>
                            <td colspan="7">
                                <table class="jobdes" border="1">
                                    <tr>
                                        <td class="subjudul" width="50%" align="center"><b>Unit Kerja<br>(Work Unit)</b></td>
                                        <td class="subjudul" width="50%" align="center"><b>Dalam Hal<br>(In Terms of)</b></td>
                                    </tr>
                                    <tr>
                                        <td class="isi">
                                            <table width="100%">
                                                <tr class="isi">
                                                    <td valign="top" width="20%" class="isi">a. Internal<br>&nbsp;&nbsp;&nbsp;&nbsp;(Internal)</td>
                                                    <td valign="top" width="1%" class="isi">: </td>
                                                    <td valign="top" class="isi"> 
                                                        @foreach ($unit as $junit)
                                                        <li>{{$junit->id_emp_cskt_ltext}}</li>
                                                        @endforeach</td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td class="isi" valign="top">
                                            <table width="100%">
                                                <tr class="isi">
                                                    <td valign="top" class="isi">@foreach ($unit as $junit)
                                                        <li>{{$junit->id_hal_internal}}</li>
                                                        @endforeach</td>
                                                </tr>
                                            </table>
                    
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="isi">
                                            <table width="100%">
                                                <tr class="isi">
                                                    <td valign="top" width="20%" class="isi">b. Eksternal<br>&nbsp;&nbsp;&nbsp;&nbsp;(External)</td>
                                                    <td valign="top" width="1%" class="isi">:</td>
                                                    <td valign="top" class="isi"> @foreach ($unit as $junit)
                                                        <li>{{$junit->id_hal_external}}</li>
                                                        @endforeach</td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td class="isi" valign="top">
                                            <table width="100%">
                                                <tr class="isi">
                                                    <td valign="top" class="isi">@foreach ($unit as $junit)
                                                       <li> {{$junit->id_eksternal}}</li>
                                                        @endforeach
                                                    </td>
                                                </tr>
                                            </table>
                    
                                        </td>
                                    </tr>
                    
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="7" class="subjudul"><b>VII. ALAT, BAHAN, DAN LINGKUNGAN KERJA (Tools, Materials, and Conditions)</b></td>
                        </tr>
                        <tr>
                                <td colspan="7" class="isi">
                                    <table width="100%">
                                        <tr class="isi">
                                            <td width="15%" class="isi">1. Alat Kerja<br>&nbsp;&nbsp;&nbsp;(Tools)</td>
                                            <td align="center" width="2%" class="isi">:</td>
                                            <td class="isi">
                                                @foreach ($tools as $jtools)
                                                <li>{{$jtools->id_deskripsi}}</li>
                                                @endforeach
                                            </td>
                                        </tr>
                                        <tr class="isi">
                                            <td width="15%" class="isi">2. Bahan Kerja<br>&nbsp;&nbsp;&nbsp;(Materials)</td>
                                            <td align="center" width="2%" class="isi">:</td>
                                            <td class="isi">@foreach ($mat as $jmat)
                                                <li>{{$jmat->id_deskripsi}}</li>
                                                @endforeach</td>
                                        </tr>
                                        <tr class="isi">
                                            <td width="15%" class="isi">3. Lingk. Kerja<br> &nbsp;&nbsp;&nbsp;(Conditions)</td>
                                            <td align="center" width="2%" class="isi">:</td>
                                            <td class="isi">@foreach ($co as $jco)
                                                <li>{{$jco->id_deskripsi}}</li>
                                                @endforeach</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="7" class="subjudul"><b>VIII. PERSYARATAN JABATAN (Job Specifications)</b></td>
                            </tr>
                            <tr>
                                    <td  class="isi" colspan="7" valign="top">
                                        <table width="100%">
                                            <tr class="isi">
                                                <td  class="isi" width="20%">1. Pendidikan dan Pengalaman Kerja <br>&nbsp;&nbsp;&nbsp;(Education & Work Experience)</td>
                                                 <td align="center" width="2%" class="isi">:</td>
                                                 <td class="isi" width="20%">
                                                        @foreach ($pen as $jpen)
                                                        <li>{{$jpen->id_jenjang}}</li>                                                     </li>
                                                        @endforeach
                                                    </td>
                                                 
                                                    <td  class="isi" > 
                                                        @foreach ($ker as $jker) 
                                                        <li>{{$jker->id_keterangan}}</li>
                                                        @endforeach                 
                        
                                                    </td>
                                            </tr>
                                        </table>
                                    </td>
                                    
                                </tr>
                                <tr>
                                        <td colspan="7" class="isi">
                                            <table width="100%">
                                                <tr class="isi">
                                                    <td width="20%" class="isi">2. Persyaratan Fisik<br> &nbsp;&nbsp;&nbsp;(Physical Requirements)</td>
                                                    <td align="center" width="2%" class="isi">:</td>
                                                    <td class="isi">
                                                        
                                                            @foreach ($fisik as $jfisik)
                                                            <li>{{$jfisik->id_persyaratan}}
                                                            </li>
                                                            @endforeach
                                                    
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                            <td colspan="7" class="isi">
                                                <table width="100%">
                                                    <tr class="isi">
                                                        <td width="20%" class="isi">3. Profil Jabatan<br> &nbsp;&nbsp;&nbsp;(Job Profile)</td>
                                                        <td align="center" width="2%" class="isi">:</td>
                                                        <td class="isi">Terlampir (Attached)</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                <tr>
                                        <td colspan="7" class="subjudul"><b>IX. POSISI JABATAN DALAM STRUKTUR (Organizational Structure)</b></td>
                                    </tr>
                                    <tr>
                                        <td colspan="7" align="center"><img src="{{ url('storage/'.$data[0]->gambar) }}" height="300" width="600" /></td>
                                    </tr>
                                    <tr>
                                        <td colspan="7" class="subjudul"><b>X. LEGALISASI (Legalization)</b></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" class="isi" align="center">DIANALISIS OLEH:<br>(Analyzed By)</td>
                                        <td colspan="4" class="isi" align="center">MENYETUJUI<br>(Approved By)</td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" class="isi" align="center"><br><br><br>@if($data[0]->approveanalis==1)<p><b>APPROVED</b></p>@endif<br>{{$data[0]->analis}}</td>
                                        <td  class="isi" align="center"><br><br><br>@if($data[0]->approveodhcp==1)<p><b>APPROVED</b></p>@endif<br>{{$data[0]->approve}}</td>
                                        <td colspan="3" class="isi" align="center"><br><br><br>@if($data[0]->approveuser==1)<p><b>APPROVED</b></p>@endif<br>{{$data[0]->namauser}}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" class="isi" align="center">{{$data[0]->jabanalis}}</td>
                                        <td  class="isi" align="center">{{$data[0]->jabapprove}}</td>
                                        <td colspan="3"class="isi" align="center">{{$data[0]->jabuser}}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" class="isi">TGL(Date): {{$data[0]->tglapproveanalis}}</td>
                                        <td  class="isi" align="center">TGL(Date):{{$data[0]->tglapproveodhcp}}</td>
                                        <td colspan="3" class="isi" align="center">TGL(Date):{{$data[0]->tglapproveuser}}</td>
                                    </tr>

                </table>
            </div>
        </div>
        <div class="box box-warning" >
            <div class="box-body" >
                <table width="100%" border="0">
                        <tr>
                            <td width="30%">
                                <img src="{{ url('img/logo.png') }}"  width="100" />
                            </td>
                            <td width="40%" align="center" valign="bottom"><strong>PROFILE JABATAN</strong></td>
                            <td width="30%" align="right">
                                <table frame="box" width="100%">
                                    <tr class="isiprofil">
                                        <td class="isi">No. Issue</td>
                                        <td class="isi">:</td>
                                        <td class="isi"></td>
                                    </tr>
                                    <tr class="isiprofil">
                                        <td class="isi">No. RS</td>
                                        <td class="isi">:</td>
                                        <td class="isi">RS/PO00/010</td>
                                    </tr>
                                    <tr class="isiprofil">
                                        <td class="isi">Tgl.</td>
                                        <td class="isi">:</td>
                                        <td class="isi">{{date("d/m/Y")}}</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <table class="jobdes" border="1">
                            <tr>
                                <td width="50%">
                                    <table>
                                        <tr class="isiprofil">
                                            <td class="isi">NAMA POSISI</td>
                                            <td class="isi">:</td>
                                            <td class="isi"><b>{{$profil[0]->namajabatan}}</b></td>
                                        </tr>
                                        <tr class="isiprofil">
                                            <td class="isi">GOLONGAN</td>
                                            <td class="isi">:</td>
                                            <td class="isi"><b>{{$profil[0]->golongan}}</b></td>
                                        </tr>
                                        <tr class="isiprofil">
                                            <td class="isi">NO. JABATAN</td>
                                            <td class="isi">:</td>
                                            <td class="isi"><b>{{$profil[0]->nojabatan}}</b></td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="50%">
                                    <table>
                                        <tr class="isiprofil">
                                            <td class="isi">ABREVIATION NO.</td>
                                            <td class="isi">:</td>
                                            <td class="isi"><b>{{$profil[0]->noorg}}</b></td>
                                        </tr>
                                        <tr class="isiprofil">
                                            <td class="isi">UNIT KERJA</td>
                                            <td class="isi">:</td>
                                            <td class="isi"><b>{{$profil[0]->unitkerja}}</b></td>
                                        </tr>
                                        <tr class="isiprofil">
                                            <td class="isi">JOB GROUP</td>
                                            <td class="isi">:</td>
                                            <td class="isi"><b>{{$profil[0]->jobgroup}}</b></td>
                                        </tr>
                                    </table>
                        
                                </td>
                            </tr>
                        </table>
                        <table class="jobdes" border="1" width="100%">
                            <tr>
                                <td align="center" style="padding:5px;" class="judulprofil"><b>KOMPETENSI</b></td>
                            </tr>
                        </table><br>
                        <table class="jobdes" border="1">
                                <tr style="color: #fff; background: black;text-align:center;padding:5px;" class="judulprofil">
                                    <th style="padding:5px;">NO</th>
                                    <th style="padding:5px;">GROUP ASPEK</th>
                                    <th style="padding:5px;">NAMA KOMPETENSI</th>
                                    <th style="padding:5px;">PROFISIENSI</th>
                                </tr>
                                <?php $no=0;?>
                                @foreach ($profil_d as $profd)
                                <?php $no++;?>
                                <tr class="isiprofil">
                                    <td align="center">{{$no}}</td>
                                    <td class="isi">{{$profd->groupaspek}}</td>
                                    <td class="isi">{{$profd->namakompetensi}}</td>
                                    <td class="isi">{{$profd->proficiency}}</td>
                                </tr>
                                @endforeach
                            </table><br>
                            <table class="jobdes" border="1" width="100%">
                                    <tr class="judulprofil">
                                        <td align="center">Disiapkan Oleh</td>
                                        <td colspan="2" align="center">Menyetujui</td>
                                    </tr>
                                    <tr class="judulprofil">
                                        <td align="center" width="33%"><br><br>@if($data[0]->approveanalis==1)<p><b>APPROVED</b></p>@endif<br>{{$data[0]->analis}}</td>
                                        <td align="center" width="33%"><br><br><br>@if($data[0]->approveodhcp==1)<p><b>APPROVED</b></p>@endif<br>{{$data[0]->approve}}</td>
                                        <td align="center" width="33%"><br><br><br>@if($data[0]->approveuser==1)<p><b>APPROVED</b></p>@endif<br>{{$data[0]->namauser}}</td>
                                    </tr>
                                    <tr class="judulprofil">
                                        <td align="center">{{$data[0]->jabanalis}}</td>
                                        <td align="center">{{$data[0]->jabapprove}}</td>
                                        <td align="center">{{$data[0]->jabuser}}</td>
                                    </tr>
                                </table>
                
                
            </div>
        </div>

    </div>
</section>
@endsection